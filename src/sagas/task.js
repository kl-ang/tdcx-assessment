import { put, call, takeLatest, all, fork } from 'redux-saga/effects';
import Actions from '../actions';
import Api from '../api'

function* task() {
  try {
    const response = yield call(Api.taskList);
    yield put(Actions.taskSuccess(response));
  } catch (error) {
    yield put(Actions.taskFail(error));
  }
}

function* taskUpdate({ params }) {
  try {
    const response = yield call(Api.taskUpdate, params);
    yield put(Actions.taskUpdateSuccess(response));
  } catch (error) {
    yield put(Actions.taskDeleteFail(error));
  }
}

function* taskCreate({ params }) {
  try {
    const response = yield call(Api.taskCreate, params);
    yield put(Actions.taskCreateSuccess(response));
  } catch (error) {
    yield put(Actions.taskCreateFail(error));
  }
}

function* taskDelete({ params }) {
  try {
    const response = yield call(Api.taskDelete, params);
    yield put(Actions.taskDeleteSuccess(response));
  } catch (error) {
    yield put(Actions.taskDeleteFail(error));
  }
}

function* watchTask() {
  yield takeLatest(Actions.TASK, task);
}

function* watchTaskUpdate() {
  yield takeLatest(Actions.TASK_UPDATE, taskUpdate);
}

function* watchTaskCreate() {
  yield takeLatest(Actions.TASK_CREATE, taskCreate);
}

function* watchTaskDelete() {
  yield takeLatest(Actions.TASK_DELETE, taskDelete);
}

export default function* taskList() {
  yield all([
    fork(watchTask),
    fork(watchTaskUpdate),
    fork(watchTaskCreate),
    fork(watchTaskDelete),
  ]);
}