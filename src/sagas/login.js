import { put, call, takeLatest, all, fork } from 'redux-saga/effects';
import Actions from '../actions';
import Api from '../api'

function* login({ params, callback }) {
  try {
    const response = yield call(Api.login, params.id);
    if (response.name === params.name) {
      yield put(Actions.loginSuccess(response));
      yield call(callback);
    } else {
      yield put(Actions.loginFail('Credential not match.'));
    }
  } catch (error) {
    yield put(Actions.loginFail(error));
  }
}

function* watchLogin() {
  yield takeLatest(Actions.LOGIN, login);
}

export default function* logon() {
  yield all([
    fork(watchLogin),
  ]);
}