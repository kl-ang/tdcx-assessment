import { fork, all } from 'redux-saga/effects';
import login from './login';
import task from './task';

export default function* root() {
  yield all([
    fork(login),
    fork(task),
  ]);
}