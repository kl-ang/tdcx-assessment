import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

function CreateModal(props) {
    const inputTextHandler = e => {
        props.setInputText(e.target.value);
    }

    const submitTodoHandler = e => {
        props.setListingTask(
            [
                ...props.listingTask,
                { 'id': `'${props.listingTask.length + 1}'`, 'name': props.inputText, 'completed': false, 'createdAt': new Date().toISOString() },
            ]
        )
        // Dispatch create task
        props.createTask({ 'id': props.listingTask.length + 1, 'name': props.inputText, 'completed': false, 'createdAt': new Date().toISOString() });
    }

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            size="sm"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Body>
                <h4 className="text-secondary pb-3">+ New Task</h4>
                <p>
                    <input onChange={inputTextHandler} type="text" placeholder="Task Name" className="w-100" />
                </p>
                <Button onClick={() => { props.onHide(); submitTodoHandler(); }} className="w-100">+ New Task</Button>
            </Modal.Body>
        </Modal >
    );
}

export default CreateModal;