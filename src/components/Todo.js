import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import { PencilFill } from 'react-bootstrap-icons';
import { TrashFill } from 'react-bootstrap-icons';
import styled from 'styled-components';
import UpdateModal from './UpdateModal';

const Label = styled.label`
    &.completed{
        text-decoration: line-through;
        color: #5d5d5d!important;
    }
`

function Todo({ task, handleInputChange, listingTask, setListingTask, deleteTask, inputText, setInputText, updateTask }) {

    const [modalShow, setModalShow] = React.useState(false);

    const deleteHandler = () => {
        setListingTask(listingTask.filter((ts) => ts.id !== task.id));
        deleteTask(task);
    }

    return (
        <>
            <ListGroup.Item>
                <input
                    name={task.name}
                    type="checkbox"
                    value={task.id}
                    onChange={handleInputChange}
                    checked={task.completed}
                    className="align-middle mr-2"
                />
                <Label className={`${task.completed ? "completed" : ''} text-primary`}>
                    {task.name}
                </Label>
                <div className="float-right text-secondary">
                    <span role="button" onClick={() => setModalShow(true)}><PencilFill /></span>
                    <span role="button" className="pl-3" onClick={deleteHandler}><TrashFill /></span>
                </div>
            </ListGroup.Item >
            <UpdateModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                inputText={inputText}
                setInputText={setInputText}
                listingTask={listingTask}
                setListingTask={setListingTask}
                updateTask={updateTask}
                task={task}
            />
        </>
    );
}

export default Todo;