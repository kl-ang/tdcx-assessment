import React, { Component } from 'react';
import { Pie } from 'react-chartjs-2';

class PieChart extends Component {
    render() {
        const { completedTask, totalTask } = this.props;
        const data = {
            labels: ['Completed Tasks', 'Uncompleted Tasks'],
            datasets: [
                {
                    backgroundColor: [
                        '#416de7',
                        '#e2e7e7',
                    ],
                    data: [completedTask, totalTask - completedTask]
                }
            ]
        };
        return (
            <div>
                <Pie
                    width={100}
                    height={40}
                    data={data}
                    options={{
                        legend: {
                            display: false,
                        }
                    }}
                />
            </div>
        );
    }
}

export default PieChart;