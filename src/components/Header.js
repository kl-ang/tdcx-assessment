import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';

function Header({ loginData, logout }) {
    return (
        <Navbar bg="light">
            <Container className="px-3">
                <Navbar.Brand>
                    <img
                        src="/profile.png"
                        width="30"
                        alt="Profile"
                    />
                </Navbar.Brand>
                <Navbar.Text>{loginData.name}</Navbar.Text>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text role="button" onClick={logout}>
                        Logout
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;
