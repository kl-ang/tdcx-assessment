import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

function UpdateModal(props) {

    const inputTextHandler = e => {
        props.setInputText(e.target.value);
    }

    const updateTodoHandler = e => {
        props.setListingTask(props.listingTask.map(item => {
            if (item.id === props.task.id) {
                return {
                    ...item, name: props.inputText
                }
            }
            return item;
        }));
        // Dispatch update task
        props.updateTask(props.task);
    }

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            size="sm"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Body>
                <h4 className="text-secondary pb-3">Update Task</h4>
                <p>
                    <input onChange={inputTextHandler} type="text" placeholder="Task Name" className="w-100" />
                </p>
                <Button onClick={() => { props.onHide(); updateTodoHandler(); }} className="w-100">Update Task</Button>
            </Modal.Body>
        </Modal >
    );
}

export default UpdateModal;