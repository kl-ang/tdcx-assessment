import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Actions from '../../actions';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import CardDeck from 'react-bootstrap/CardDeck';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import FormControl from 'react-bootstrap/FormControl';
import styled from 'styled-components';
import { PieChart, CreateModal, Todo, Header } from '../../components';
import { saveLocalStorage, getLocalStorage } from '../../utils/localStorage';

const Li = styled.li`
    &.completed{
        text-decoration: line-through;
    }
`

function Dashboard({ history }) {
    const totalTask = 20;
    const dispatch = useDispatch();
    const taskData = useSelector(state => state.task.taskList.data);

    // State
    const [loginData, setLoginData] = useState({ id: "", name: "" });
    const [filterText, setFilterText] = useState('');
    const [listingTask, setListingTask] = useState([]);
    const [modalShow, setModalShow] = React.useState(false);
    const [inputText, setInputText] = useState('');
    const [filteredTask, setFilteredTask] = useState([]);

    useEffect(() => {
        dispatch(Actions.task());
        getLogin();
    }, [dispatch]);

    useEffect(() => {
        setListingTask(taskData);
    }, [taskData]);

    useEffect(() => {
        setFilteredTask(listingTask);
        setFilterText('');
    }, [listingTask]);

    // Dispatch function
    const createTask = data => {
        dispatch(Actions.taskCreate(data));
    }

    const deleteTask = data => {
        dispatch(Actions.taskDelete(data));
    }

    const updateTask = data => {
        dispatch(Actions.taskUpdate(data));
    }

    // Function
    // Get login info from Local Storage, if empty redirect to login page
    const getLogin = () => {
        let storageLogin = getLocalStorage('login');
        if (typeof storageLogin === 'undefined' || storageLogin?.length <= 0) {
            history.push('/')
        }
        setLoginData(storageLogin);
    }

    const logout = () => {
        alert('Logout Successful!');
        saveLocalStorage('login', [])
        history.push('/')
    }

    // Filter task list function
    const filterHandler = e => {
        const target = e.target;
        setFilterText(target.value);
        setFilteredTask(listingTask.filter((task) => task.name.includes(target.value)));
    }

    // Handle checkbox for task list
    const handleInputChange = e => {
        const target = e.target;
        setListingTask(listingTask.map((item) => {
            if (item.id === target.value) {
                return {
                    ...item, completed: !item.completed
                }
            }
            return item;
        }))
    }

    // Sort Task list by date
    let sortedTaskByDate = listingTask.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
    // Calculate total complete task
    let completedTask = listingTask.filter(task => {
        return task.completed;
    });

    return (
        <>
            <Header loginData={loginData} logout={logout} />
            <Container className="my-3 pb-3">
                {listingTask?.length > 0 ? (
                    <>
                        <Row>
                            <Col xs="12" md="12" lg="12">
                                <CardDeck>
                                    <Card>
                                        <Card.Body>
                                            <Card.Title>Tasks Completed</Card.Title>
                                            <span className="text-primary display-4">{completedTask.length > 0 ? completedTask.length : 0}</span> / <span className="text-secondary">{totalTask}</span>
                                        </Card.Body>
                                    </Card>
                                    <Card>
                                        <Card.Body>
                                            <Card.Title>Latest Created Tasks</Card.Title>
                                            <ul className="pl-3">
                                                {
                                                    sortedTaskByDate.length > 0 && sortedTaskByDate.slice(0, 3).map((task, index) => (
                                                        <Li className={`text-secondary ${task.completed ? 'completed' : ''}`} key={index}>
                                                            <span key={index}>{task.name}</span>
                                                        </Li>
                                                    ))
                                                }
                                            </ul>
                                        </Card.Body>
                                    </Card>
                                    <Card>
                                        <Card.Body>
                                            <PieChart completedTask={completedTask.length} totalTask={totalTask} />
                                        </Card.Body>
                                    </Card>
                                </CardDeck>
                            </Col>
                        </Row>

                        <Row className="mt-3">
                            <Col xs>
                                <Form inline className="d-block d-sm-flex">
                                    <div className="text-center">Tasks</div>
                                    <div className="ml-auto">
                                        <FormControl type="text" placeholder="Search by task name" className="mr-sm-2 my-2 my-sm-0" onChange={filterHandler} value={filterText} />
                                        <Button variant="primary" onClick={() => setModalShow(true)} className="btn-xs-block">+ New Task</Button>
                                    </div>
                                </Form>
                            </Col>
                        </Row>

                        <Row className="mt-3">
                            <Col xs>
                                <Card>
                                    <ListGroup>
                                        {
                                            filteredTask.map((task, index) => (
                                                <Todo
                                                    key={`todo-${index}`}
                                                    task={task}
                                                    handleInputChange={handleInputChange}
                                                    listingTask={listingTask}
                                                    setListingTask={setListingTask}
                                                    updateTask={updateTask}
                                                    deleteTask={deleteTask}
                                                    inputText={inputText}
                                                    setInputText={setInputText}
                                                />
                                            ))
                                        }
                                    </ListGroup>
                                </Card>
                            </Col>
                        </Row>

                        <CreateModal
                            show={modalShow}
                            onHide={() => setModalShow(false)}
                            inputText={inputText}
                            setInputText={setInputText}
                            listingTask={listingTask}
                            setListingTask={setListingTask}
                            createTask={createTask}
                        />
                    </>
                ) : (<>
                    {/* Show when no task available */}
                    <Row className="justify-content-md-center h-100">
                        <Col xs="12" md="4" lg="4" className="my-auto text-center text-secondary">
                            <Card>
                                <Card.Body>
                                    <Card.Title>You have no task.</Card.Title>
                                    <Button variant="primary" type="submit" onClick={() => setModalShow(true)}>
                                        + New Task
                                        </Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    <CreateModal
                        show={modalShow}
                        onHide={() => setModalShow(false)}
                        inputText={inputText}
                        setInputText={setInputText}
                        listingTask={listingTask}
                        setListingTask={setListingTask}
                        createTask={createTask}
                    />
                </>)
                }
            </Container>
        </>
    );
}

export default Dashboard;
