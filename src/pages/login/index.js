import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Actions from '../../actions';
import { saveLocalStorage } from '../../utils/localStorage';

function Login({ history }) {

    // State
    const [user, setUser] = useState({ id: "", name: "" });
    const error = useSelector(state => state.login.errors);
    const dispatch = useDispatch();

    // Function
    const handleSubmit = e => {
        e.preventDefault();
        dispatch(Actions.login(user, redirectToDashboard));
    }

    const redirectToDashboard = () => {
        alert('Login Successful!');
        saveLocalLogin();
        history.push('/dashboard')
    }

    // Save login info to Local Storage
    const saveLocalLogin = () => {
        saveLocalStorage('login', user);
    }

    return (
        <Container fluid="true h-100">
            <Row className="justify-content-md-center h-100">
                <Col xs="12" md="4" lg="4" xl="3" className="my-sm-auto m-2">
                    <Card>
                        <Card.Body>
                            <Card.Title>Login</Card.Title>
                            <Form onSubmit={handleSubmit}>
                                <Form.Group controlId="formName">
                                    <Form.Control type="text" name="id" placeholder="Id" onChange={e => setUser({ ...user, id: e.target.value })} value={user.id} />
                                </Form.Group>
                                <Form.Group controlId="formPassword">
                                    <Form.Control type="text" name="name" placeholder="Name" onChange={e => setUser({ ...user, name: e.target.value })} value={user.name} />
                                </Form.Group>
                                <Button variant="primary" type="submit" block>
                                    Login
                                </Button>
                            </Form>
                            <p className="text-danger mt-3">
                                {Object.keys(error).length !== 0 && 'Login failed. Please check your detail.'}
                            </p>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default Login;
