import { createStore, applyMiddleware, compose } from 'redux';
import { combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const combineRootReducer = combineReducers(rootReducer);
const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
    let store = createStore(combineRootReducer, composeEnhancer(applyMiddleware(sagaMiddleware)));
    sagaMiddleware.run(rootSaga)
    return store;
};

export default configureStore;