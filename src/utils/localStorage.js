
export const saveLocalStorage = (name, value) => {
    localStorage.setItem(name, JSON.stringify(value));
};

export const getLocalStorage = (name) => {
    if (localStorage.getItem(name) === null) {
        localStorage.setItem(name, JSON.stringify([]));
    } else {
        return JSON.parse(localStorage.getItem(name));
    }
};