
export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const login = (params, callback) => ({
    type: LOGIN,
    params,
    callback,
});

export const loginSuccess = data => ({
    type: LOGIN_SUCCESS,
    data,
});

export const loginFail = errors => ({
    type: LOGIN_FAIL,
    errors,
});
