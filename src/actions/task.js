
export const TASK = 'TASK';
export const TASK_SUCCESS = 'TASK_SUCCESS';
export const TASK_FAIL = 'TASK_FAIL';
export const TASK_UPDATE = 'TASK_UPDATE';
export const TASK_UPDATE_SUCCESS = 'TASK_UPDATE_SUCCESS';
export const TASK_UPDATE_FAIL = 'TASK_UPDATE_FAIL';
export const TASK_CREATE = 'TASK_CREATE';
export const TASK_CREATE_SUCCESS = 'TASK_CREATE_SUCCESS';
export const TASK_CREATE_FAIL = 'TASK_CREATE_FAIL';
export const TASK_DELETE = 'TASK_DELETE';
export const TASK_DELETE_SUCCESS = 'TASK_DELETE_SUCCESS';
export const TASK_DELETE_FAIL = 'TASK_DELETE_FAIL';

export const task = () => ({
    type: TASK,
});

export const taskSuccess = data => ({
    type: TASK_SUCCESS,
    data,
});

export const taskFail = errors => ({
    type: TASK_FAIL,
    errors,
});

export const taskUpdate = (params) => ({
    type: TASK_UPDATE,
    params
});

export const taskUpdateSuccess = data => ({
    type: TASK_UPDATE_SUCCESS,
    data,
});

export const taskUpdateFail = errors => ({
    type: TASK_UPDATE_FAIL,
    errors,
});


export const taskCreate = (params) => ({
    type: TASK_CREATE,
    params
});

export const taskCreateSuccess = data => ({
    type: TASK_CREATE_SUCCESS,
    data,
});

export const taskCreateFail = errors => ({
    type: TASK_CREATE_FAIL,
    errors,
});

export const taskDelete = (params) => ({
    type: TASK_DELETE,
    params
});

export const taskDeleteSuccess = data => ({
    type: TASK_DELETE_SUCCESS,
    data,
});

export const taskDeleteFail = errors => ({
    type: TASK_DELETE_FAIL,
    errors,
});