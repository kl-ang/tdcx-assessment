import * as login from './login';
import * as task from './task';

export default {
    ...login,
    ...task,
}