import login from './login';
import task from './task';

export default {
    login,
    task,
};
