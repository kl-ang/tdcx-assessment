import Actions from '../actions';

function login(state = { errors: {}, data: {} }, action) {
  switch (action.type) {
    case Actions.LOGIN:
      return {
        errors: {},
        data: {}
      };
    case Actions.LOGIN_SUCCESS:
      return {
        errors: {},
        data: action.data,
      };
    case Actions.LOGIN_FAIL:
      return {
        errors: action.errors,
        data: {},
      };
    default:
      return state;
  }
}

export default login;