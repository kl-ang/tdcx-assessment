import Actions from '../actions';
import { combineReducers } from 'redux';

function taskList(state = { errors: {}, data: [] }, action) {
  switch (action.type) {
    case Actions.TASK:
      return {
        errors: {},
        data: []
      };
    case Actions.TASK_SUCCESS:
      return {
        errors: {},
        data: action.data,
      };
    case Actions.TASK_FAIL:
      return {
        errors: action.errors,
        data: [],
      };
    default:
      return state;
  }
}

function taskUpdate(state = { errors: {}, data: [] }, action) {
  switch (action.type) {
    case Actions.TASK_UPDATE:
      return {
        errors: {},
        data: []
      };
    case Actions.TASK_UPDATE_SUCCESS:
      return {
        errors: {},
        data: action.data,
      };
    case Actions.TASK_UPDATE_FAIL:
      return {
        errors: action.errors,
        data: [],
      };
    default:
      return state;
  }
}

function taskCreate(state = { errors: {}, data: [] }, action) {
  switch (action.type) {
    case Actions.TASK_CREATE:
      return {
        errors: {},
        data: []
      };
    case Actions.TASK_CREATE_SUCCESS:
      return {
        errors: {},
        data: action.data,
      };
    case Actions.TASK_CREATE_FAIL:
      return {
        errors: action.errors,
        data: [],
      };
    default:
      return state;
  }
}

function taskDelete(state = { errors: {}, data: [] }, action) {
  switch (action.type) {
    case Actions.TASK_DELETE:
      return {
        errors: {},
        data: []
      };
    case Actions.TASK_DELETE_SUCCESS:
      return {
        errors: {},
        data: action.data,
      };
    case Actions.TASK_DELETE_FAIL:
      return {
        errors: action.errors,
        data: [],
      };
    default:
      return state;
  }
}

export default combineReducers({
  taskList,
  taskUpdate,
  taskCreate,
  taskDelete,
})