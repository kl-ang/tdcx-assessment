// import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './custom.scss';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import Login from './pages/login';
import Dashboard from './pages/dashboard';

function App() {
  return (
    <Router>
      <Route exact path="/" component={Login} />
      <Route exact path="/dashboard" component={Dashboard} />
    </Router>
  );
}

export default App;
