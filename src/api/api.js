const fetchUrl = (method, endpoint, body = {}) => {
    const parameters = {
        method: method,
        headers: {
            body: JSON.stringify(body)
        },
    };
    return fetch(endpoint, parameters)
        .then(response => {
            return response.json()
        })
        .catch(error => {
            return error;
        });
};

const api = {
    post(endpoint, params) {
        return fetchUrl('post', endpoint, params);
    },
    get(endpoint, params) {
        return fetchUrl('get', endpoint, params);
    },
    put(endpoint, params) {
        return fetchUrl('put', endpoint, params);
    },
    delete(endpoint, params) {
        return fetchUrl('delete', endpoint, params);
    },
};

export default api;