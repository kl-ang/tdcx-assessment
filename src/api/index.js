import api from './api';

export default {
  login: (data) =>
    api.get('https://6059c234b11aba001745cd2f.mockapi.io/login/' + data),
  taskList: () =>
    api.get('https://6059c234b11aba001745cd2f.mockapi.io/tasks'),
  taskCreate: (data) =>
    api.post('https://6059c234b11aba001745cd2f.mockapi.io/tasks/', data),
  taskUpdate: (data) =>
    api.put('https://6059c234b11aba001745cd2f.mockapi.io/tasks/' + parseInt(data.id), data),
  taskDelete: (data) =>
    api.delete('https://6059c234b11aba001745cd2f.mockapi.io/tasks/' + data.id),
};