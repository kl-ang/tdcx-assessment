# Getting Started with TDCX Assessment

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to start project

In the project directory, you can run:

1. `yarn install`

To install npm package.

2. `yarn start` 

To start testing project in local.

## Login Credential
Please use this credential to login the application.

ID: `1` 

Name: `tdcx-name`

## Api Documentation
API is using [mockapi.io](mockapi.io) for this project.
### Endpoint and Schema
2 APIs is using here:
```
# To retrieve login credential
https://6059c234b11aba001745cd2f.mockapi.io/login
```
Field | Type |
--- | --- |
Id | ID |
name | string |
password | string |
createdAt | date |
```
# To perform get, update, create and delete task
https://6059c234b11aba001745cd2f.mockapi.io/task
```
Field | Type |
--- | --- |
Id | ID |
name | string |
completed | boolean |
createdAt | date |

## API Note:
The actual data will be lose after refreshing the page because is it using mock data. However, the record will still be create and update to the API.